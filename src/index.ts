import express from 'express';
import * as TikTokScraper from 'tiktok-scraper';

const app = express();

app.get('/tiktok-user-profile', async (req, res) => {
    try {
        const username = <string>req.query.username;
        const user = await TikTokScraper.getUserProfileInfo(username, {});
        res.send(user);
    }
    catch (ex) {
        res.send('An error occured while processing your request');
    }
})

app.get('/', (req, res) => {
    res.send('Server running');
})

app.listen(3000, () => {
    console.log('The application is listening on port 3000!');
})